#ifndef ROBOT_H
#define ROBOT_H

#include "FSM/statemachine.h"

constexpr double ROBOT_RADIUS = 100.0;

struct Robot
{
    int x;
    int y;
    StateMachine<Robot> m;
    Robot();
};

#endif