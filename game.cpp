#include "game.h"
#include "robot.h"
#include "FSM/statemachine.h"
#include "FSM/Game/gamestates.h"

Game::Game() : robot(), m(this)
{
    m.setCurrentState(GameStates<Halt>::Instance());
    m.setPreviousState(GameStates<Halt>::Instance());
}