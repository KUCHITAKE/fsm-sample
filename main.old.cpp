#include <iostream>
#include <array>

constexpr double ROBOT_RADIUS = 100.0;

template <class T>
class State
{
public:
    virtual void enter(T *) = 0;
    virtual void execute(T *) = 0;
    virtual void exit(T *) = 0;

    virtual ~State() {}
};

struct Game;
struct Halt
{
    static void execute(Game* me)
    {
        std::cout << "HALT" << std::endl;
        me->robot[0].m.changeState(RobotStates<Attacker<Halt>>::Instance());
        me->robot[1].m.changeState(RobotStates<Attacker<Halt>>::Instance());
        me->robot[2].m.changeState(RobotStates<Attacker<Halt>>::Instance());
    }
    static constexpr double maxSpeed = 0.0;
};

struct Stop
{
    static void execute(Game* me)
    {
        std::cout << "STOP" << std::endl;
    }
    static constexpr double maxSpeed = 1000.0;
};

struct Inplay
{
    static void execute(Game* me)
    {
        std::cout << "INPLAY" << std::endl;
    }
    static constexpr double maxSpeed = 3000.0;
};

struct Robot;
template <class T>
struct Attacker
{
    static constexpr int value(){return 0;};
    static void execute(Robot* me) {std::cout << "default attacker execute  param:" << value() << std::endl;}
};

template<>
constexpr int Attacker<Stop>::value()
{
    return 10;
}

template<>
constexpr int Attacker<Halt>::value()
{
    return 100;
}

template<>
constexpr int Attacker<Inplay>::value()
{
    return 300;
}

template<>
void Attacker<Halt>::execute(Robot* me) {
    std::cout << "attacker halt custum execute param:" << value() << std::endl;
}

constexpr int DEFENDER_MAX = 2;
template <class T, int num>
struct Defender
{
    static_assert(num < DEFENDER_MAX, "defender id must smaller than 2");
    static void execute(Robot* me) {std::cout << "deffff" << num << std::endl;}
};


template <class T>
struct Defender<T, 0>
{
    static void execute(Robot* me) {std::cout << "defender default execute" << 0 << std::endl;}
};

template <class T>
struct Defender<T, 1>
{
    static void execute(Robot* me) {std::cout << "defender default execute" << 1 << std::endl;}
};


template <>
void Defender<Halt, 1>::execute(Robot* me)
{
    std::cout << "def halt 1 custom  execute" << std::endl;
}

template <class T>
class GameStates : public State<Game>
{
    private:
    GameStates(){};
    public:
    static GameStates* Instance()
    {
        static GameStates<T> instance;
        return &instance;
    }
    void enter(Game *me)
    {
    }
    void execute(Game *me)
    {
        T::execute(me);
    }
    void exit(Game *me)
    {
    }

    ~GameStates() {}
};
template <class T>
class RobotStates : public State<Robot>
{
private:
    RobotStates(){};

public:
    static RobotStates *Instance()
    {
        static RobotStates<T> instance;
        return &instance;
    }
    void enter(Robot *me)
    {
    }
    void execute(Robot *me)
    {
        T::execute(me);
    }
    void exit(Robot *)
    {
    }

    ~RobotStates() {}
};

template <class T>
class StateMachine
{
public:
    StateMachine(T* owner) : m_owner(owner),
            m_cuurentState(nullptr),
            m_previousState(nullptr),
            m_globalState(nullptr)
    {}

    virtual ~StateMachine(){}

    void update() const
    {
        if(m_globalState) m_globalState->execute(m_owner);

        if(m_cuurentState) m_cuurentState->execute(m_owner);
    }

    void changeState(State<T> *newState)
    {
        m_previousState = m_cuurentState;

        m_cuurentState->exit(m_owner);

        m_cuurentState = newState;

        m_cuurentState->enter(m_owner);
    }
    void revertToPreviousState()
    {
        changeState(m_previousState);
    }

    State<T>* currentState(){return m_cuurentState;}
    State<T>* previousState(){return m_previousState;}
    State<T>* globalState(){return m_globalState;}

    void setCurrentState(State<T>* state){m_cuurentState  = state;}
    void setPreviousState(State<T>* state){m_previousState = state;}
    void setGlobalState(State<T>* state){m_globalState = state;}

    bool isInState(const State<T>& st) const {return std::is_same(*m_cuurentState, st);}

private:
    T* m_owner;
    State<T>* m_cuurentState;
    State<T>* m_previousState;
    State<T>* m_globalState;
};

struct Robot
{
    int x;
    int y;
    StateMachine<Robot> m;
    Robot() : x(0), y(0), m(this)
    {
        m.setCurrentState(RobotStates<Attacker<Halt>>::Instance());
        m.setPreviousState(RobotStates<Attacker<Halt>>::Instance());
    };
};

struct Game
{
    std::array<Robot, 3> robot;
    StateMachine<Game> m;
    Game() : robot(), m(this)
    {
        m.setCurrentState(GameStates<Halt>::Instance());
        m.setPreviousState(GameStates<Halt>::Instance());
    };
};


int main()
{
    const std::array<State<Game>*, 3> a{
        GameStates<Halt>::Instance(),
        GameStates<Stop>::Instance(),
        GameStates<Inplay>::Instance()
    };
    Game game;
    for(auto&& b: a) {
        game.m.changeState(b);
        game.m.update();
    }
}