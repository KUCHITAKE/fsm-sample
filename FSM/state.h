#ifndef STATE_H
#define STATE_H

template <class T>
class State
{
public:
    virtual void enter(T *) = 0;
    virtual void execute(T *) = 0;
    virtual void exit(T *) = 0;

    virtual ~State() {}
};

#endif