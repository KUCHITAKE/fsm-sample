#include "attacker.h"

template<>
constexpr int Attacker<Halt>::value()
{
    return 10;
}

template<>
void Attacker<Halt>::execute(Robot* me)
{
    std::cout << "custom halt attacker execute  param:" << value() << " maxSpeed:" << Halt::maxSpeed << std::endl;
}
