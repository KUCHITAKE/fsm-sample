#ifndef ATTACKER_H
#define ATTACKER_H

#include <iostream>
#include "robotstates.h"
#include "../../robot.h"

template <class T>
struct Attacker
{
    static constexpr int value()
    {
        return 0;
    }
    static void execute(Robot *me)
    {
        std::cout << "default attacker execute  param:" << value() << " maxSpeed:" << T::maxSpeed << std::endl;
    }
};

#endif