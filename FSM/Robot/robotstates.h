#ifndef ROBOTSTATES_H
#define ROBOTSTATES_H

#include <iostream>
#include "../../robot.h"
#include "../Game/gamestates.h"

template <class T>
class RobotStates : public State<Robot>
{
private:
    RobotStates(){};

public:
    static RobotStates *Instance()
    {
        static RobotStates<T> instance;
        return &instance;
    }
    void enter(Robot *me)
    {
    }
    void execute(Robot *me)
    {
        T::execute(me);
    }
    void exit(Robot *)
    {
    }

    ~RobotStates() {}
};

/*
template<>
constexpr int Attacker<Stop>::value()
{
    return 10;
}

template<>
constexpr int Attacker<Halt>::value()
{
    return 100;
}

template<>
constexpr int Attacker<Inplay>::value()
{
    return 300;
}

template<>
void Attacker<Halt>::execute(Robot* me) {
    std::cout << "attacker halt custum execute param:" << value() << std::endl;
}
*/

constexpr int DEFENDER_MAX = 2;
template <class T, int num>
struct Defender
{
    static_assert(num < DEFENDER_MAX, "defender id must smaller than 2");
    static void execute(Robot* me) {std::cout << "deffff" << num << std::endl;}
};


/*
template <class T>
struct Defender<T, 0>
{
    static void execute(Robot* me) {std::cout << "defender default execute" << 0 << std::endl;}
};

template <class T>
struct Defender<T, 1>
{
    static void execute(Robot* me) {std::cout << "defender default execute" << 1 << std::endl;}
};


template <>
void Defender<Halt, 1>::execute(Robot* me)
{
    std::cout << "def halt 1 custom  execute" << std::endl;
}
*/

#include "attacker.h"
#endif