#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <type_traits>

#include "state.h"

template <class T>
class StateMachine
{
public:
    StateMachine(T* owner) : m_owner(owner),
            m_cuurentState(nullptr),
            m_previousState(nullptr),
            m_globalState(nullptr)
    {}

    virtual ~StateMachine(){}

    void update() const
    {
        if(m_globalState) m_globalState->execute(m_owner);

        if(m_cuurentState) m_cuurentState->execute(m_owner);
    }

    void changeState(State<T> *newState)
    {
        m_previousState = m_cuurentState;

        m_cuurentState->exit(m_owner);

        m_cuurentState = newState;

        m_cuurentState->enter(m_owner);
    }
    void revertToPreviousState()
    {
        changeState(m_previousState);
    }

    State<T>* currentState(){return m_cuurentState;}
    State<T>* previousState(){return m_previousState;}
    State<T>* globalState(){return m_globalState;}

    void setCurrentState(State<T>* state){m_cuurentState  = state;}
    void setPreviousState(State<T>* state){m_previousState = state;}
    void setGlobalState(State<T>* state){m_globalState = state;}

    bool isInState(const State<T>& st) const {return std::is_same(*m_cuurentState, st);}

private:
    T* m_owner;
    State<T>* m_cuurentState;
    State<T>* m_previousState;
    State<T>* m_globalState;
};

#endif