#include "gamestates.h"
#include "../../game.h"
#include "../Robot/robotstates.h"

#include <iostream>

void Inplay::execute(Game *me)
{
    std::cout << "Inplay" << std::endl;
    me->robot[0].m.changeState(RobotStates<Attacker<Inplay>>::Instance());
    me->robot[1].m.changeState(RobotStates<Attacker<Inplay>>::Instance());
    me->robot[2].m.changeState(RobotStates<Attacker<Inplay>>::Instance());
}