#include "gamestates.h"
#include "../../game.h"
#include "../Robot/robotstates.h"

#include <iostream>

void Halt::execute(Game *me)
{
    std::cout << "HALT" << std::endl;
    me->robot[0].m.changeState(RobotStates<Attacker<Halt>>::Instance());
    me->robot[1].m.changeState(RobotStates<Attacker<Halt>>::Instance());
    me->robot[2].m.changeState(RobotStates<Attacker<Halt>>::Instance());
}