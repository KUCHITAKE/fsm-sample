#ifndef GAMESTATES_H
#define GAMESTATES_H

#include <array>
#include "../state.h"
#include "../Robot/robotstates.h"

struct Game;

template <class T>
class GameStates : public State<Game>
{
    private:
    GameStates(){};
    public:
    static GameStates* Instance()
    {
        static GameStates<T> instance;
        return &instance;
    }
    void enter(Game *me)
    {
    }
    void execute(Game *me)
    {
        T::execute(me);
    }
    void exit(Game *me)
    {
    }

    ~GameStates() {}
};

struct Halt
{
    static void execute(Game *me);
    static constexpr double maxSpeed = 0.0;
};

struct Inplay
{
    static void execute(Game *me);
    static constexpr double maxSpeed = 3000.0;
};

struct Stop
{
    static void execute(Game *me);
    static constexpr double maxSpeed = 1000.0;
};

#endif