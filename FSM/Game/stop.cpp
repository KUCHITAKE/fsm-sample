#include "gamestates.h"
#include "../../game.h"
#include "../Robot/robotstates.h"

#include <iostream>

void Stop::execute(Game *me)
{
    std::cout << "Stop" << std::endl;
    me->robot[0].m.changeState(RobotStates<Attacker<Stop>>::Instance());
    me->robot[1].m.changeState(RobotStates<Attacker<Stop>>::Instance());
    me->robot[2].m.changeState(RobotStates<Attacker<Stop>>::Instance());
}