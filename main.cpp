
#include <array>
#include "game.h"
#include "FSM/Game/gamestates.h"

int main()
{
    const std::array<State<Game>*, 3> a{
        GameStates<Halt>::Instance(),
        GameStates<Stop>::Instance(),
        GameStates<Inplay>::Instance()
    };
    Game game;
    for(auto&& b: a) {
        game.m.changeState(b);
        game.m.update();

        for(auto&& bot: game.robot) {
            bot.m.update();
        }
    }
}