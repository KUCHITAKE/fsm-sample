#ifndef GAME_H
#define GAME_H

#include <array>

#include "robot.h"
#include "FSM/statemachine.h"

struct Game
{
    std::array<Robot, 3> robot;
    StateMachine<Game> m;
    Game();
};

#endif