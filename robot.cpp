#include "robot.h"
#include "FSM/statemachine.h"

#include "FSM/Robot/robotstates.h"

Robot::Robot() : x(0), y(0), m(this)
{
    m.setCurrentState(RobotStates<Attacker<Halt>>::Instance());
    m.setPreviousState(RobotStates<Attacker<Halt>>::Instance());
}